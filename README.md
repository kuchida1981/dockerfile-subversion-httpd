# Subversion on Apache2 (HTTP)

Subversionをサービスとして動かすためのイメージです.
Apache2に提供させるサービスとしています.

## Description

* Ubuntuのオフィシャルイメージをベースにしています
* 複数リポジトリの作成に対応しています
* 認証にはBASIC認証を用いています
* 認証可能なユーザだけが, リポジトリの閲覧と変更を行えます

## Usage

Dockerホストの10080番ポートを使って,
`tools` リポジトリを作成するコンテナを作成/開始します.

使うユーザ/パスワード `username/secret` です.

```
# docker run --rm --name local-svn -p 10080:80 \
-e SVN_DEFAULT_USER="username" \
-e SVN_DEFAULT_USER_PASSWD="secret" \
-e SVN_DEFAULT_REPOSITORY="tools" \
kuchida1981/subversion-httpd
```

バックグラウンドで立ち上げたい場合には, オプション `--rm` の代わりに,
`-d` を指定します.

Dockerサービスが起動した時点で立ち上がるようにしたい場合は,
オプション `--restart=unless-stopped` を加えます.

コンテナが起動したら, 次のことを確認してください.

* `http://127.0.0.1:10080` へアクセスし,
  リポジトリ一覧が表示され, `tools` リポジトリが含まれる

* `http://127.0.0.1:10080/tools` でリポジトリをチェックアウトできる

* チェックアウトしたリポジトリに加えた変更をコミットできる

### リポジトリを追加する

ここでは `tools2` という名前のリポジトリを新たに加える手順を示します.

リポジトリを格納しているディレクトリは `/var/svn/repos` です.
ユーザ `www-data` に所有させているので,
リポジトリを追加するためには, `www-data` 所有のリポジトリを作成する必要があります.

起動中のコンテナにログインします.

```
# docker exec -it local-svn
```

`www-data` ユーザとしてログインします.

```
# su -s /bin/bash www-data
```

リポジトリを作成し, 必要なディレクトリを作ってコミットします.

```
$ svnadmin create /var/svn/repos/tools2
$ svn mkdir file:///var/svn/repos/trunk \
    file:///var/svn/repos/branches \
    file:///var/svn/repos/tags \
    -m "Create repository."
```


### リポジトリを利用可能なユーザを追加する.

ユーザ/パスワード `tonegawa/secret` というユーザを追加する手順を示します.

起動中のコンテナにログインします.

```
# docker exec -it local-svn
```

`htpasswd` コマンドを用いて, BASIC認証できるユーザを追加します.

```
# htpasswd -cb /etc/subversion/passwd \
    tonegawa secret
```


## Environment variables

変更可能な環境変数です.

### `SVN_DEFAULT_USER`

コンテナ作成時に追加するユーザのユーザ名です.
指定しなければ, `default` ユーザを作成します.

### `SVN_DEFAULT_USER_PASSWD`

コンテナ作成時に追加するユーザのパスワードです.
指定しなければパスワード `default` とします.

### `SVN_DEFAULT_REPOSITORY`

コンテナ作成時に作成するリポジトリの名前です.
指定しなければ, `default` という名前のリポジトリを作成します.


## Dockerイメージをビルドする

`Dockerfile` からDockerイメージをビルドする手順です.

gitリポジトリをクローンします.

```
$ git clone https://kuchida1981@bitbucket.org/kuchida1981/dockerfile-subversion-httpd.git subversion-httpd
```

`docker` コマンドでビルドします.
`root` ユーザでログインできるなら, `sudo` は不要です.

```
$ sudo docker build -t subversion-httpd-debug subversion-httpd
```


## Examples

会社のチーム内等でよく用いられる, redmine+subversion
といった形態の開発体制は,
オフィシャルイメージのredmineと組み合わせて実現できます.

docker-compose.yml でまとめて面倒みるサンプルです.

```
version: '2'
services:
        db:
                image: postgres
                environment:
                        - POSTGRES_USER=redmine
                        - POSTGRES_PASSWORD=secret
                ports:
                        - "25432:5432"
                restart: unless-stopped
        vcs:
                image: kuchida1981/subversion-httpd
                ports:
                        - "20080:80"
                environment:
                        - SVN_DEFAULT_USER=kosuke
                        - SVN_DEFAULT_USER_PASSWD=kosuke0
                restart: unless-stopped
        redmine:
                image: redmine
                links:
                        - db
                        - vcs
                ports:
                        - "23000:3000"
                restart: unless-stopped
```

subversion/httpをホストOSの20080番に,
redmineのデフォルト3000番ポートをホストの23000番にポートフォワードしています.
ついでにメンテ用にpostgresqlのデフォルト 5432番もホストの25432番に転送しています.

## License

[MIT](https://github.com/tcnksm/tool/blob/master/LICENCE)


## Author

[Kosuke Uchida](https://bitbucket.org/kuchida1981/)
