#!/usr/bin/env bash

# create apache2 conf file.
cat <<EOF > /etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>
 <Location />
  DAV svn
  SVNParentPath /var/svn/repos
  SVNListParentPath on
  AuthType Basic
  AuthName "${SVN_REPOSITORIES_DESCR}"
  AuthUserFile /etc/subversion/passwd
  Require valid-user
 </Location>

 ServerAdmin webmaster@localhost
 DocumentRoot /var/www/html

 ErrorLog ${APACHE_LOG_DIR}/error.log
 CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

# create default user
htpasswd -cb /etc/subversion/passwd ${SVN_DEFAULT_USER} ${SVN_DEFAULT_USER_PASSWD}

# create repository root directory.
if [ ! -d /var/svn/repos ]; then
	mkdir -p /var/svn/repos
fi

repopath=/var/svn/repos/${SVN_DEFAULT_REPOSITORY}
if [ ! -d $repopath ]; then
	svnadmin create $repopath

	svn mkdir file://${repopath}/trunk \
		file://${repopath}/branches \
		file://${repopath}/tags \
		-m "Create repository."

	chmod -R a+wrx /var/svn/repos
fi

exec "$@"
